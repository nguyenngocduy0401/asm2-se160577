﻿using Asm2.Data;
using Asm2.Models;
using Asm2.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Asm2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        private readonly IBranchRepository _branchRepo;

        public BranchesController(IBranchRepository repo) {
        
            _branchRepo = repo;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllBranches()
        {
            try
            {
                return Ok(await  _branchRepo.GetAllBranchesAsync());
            }
            catch { 
                return BadRequest();
            }
        }

        [HttpGet("{branchId}")]
        public async Task<IActionResult> GetBranchById(int branchId)
        { 
            var branch = await _branchRepo.GetBranchAsync(branchId);
            return branch == null ? NotFound() : Ok(branch);
        }

        [HttpPost]
        public async Task<IActionResult> AddNewBranch(BranchModel model)
        {
            try
            {
                var newBranchId = await _branchRepo.AddBranchAsync(model);
                var branch = await _branchRepo.GetBranchAsync(newBranchId);
                return branch == null ? NotFound() : Ok(branch);
            }
            catch { 
                return BadRequest();
            }
        }

        [HttpPut("{branchId}")]
        public async Task<IActionResult> UpdateBranch(int branchId, BranchModel model)
        {
            if (branchId != model.BranchId) { 
                return NotFound();
            }
            await _branchRepo.UpdateBranchAsync(branchId, model);
            return Ok();
        }

        [HttpDelete("{branchId}")]
        public async Task<IActionResult> DeleteBranch(int branchId)
        {
            await _branchRepo.DeleteBranchAsync(branchId);
            return Ok();
        }
    }
}
