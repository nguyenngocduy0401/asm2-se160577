﻿using Asm2.Data;
using Asm2.Models;
using AutoMapper;

namespace Asm2.Helpers
{
    public class ApplicationMapper : Profile
    {
        public ApplicationMapper() {
            CreateMap<Branch, BranchModel>().ReverseMap();
        }
    }
}
