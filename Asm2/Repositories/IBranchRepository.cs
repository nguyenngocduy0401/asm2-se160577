﻿using Asm2.Data;
using Asm2.Models;

namespace Asm2.Repositories
{
    public interface IBranchRepository
    {
        public Task<List<BranchModel>> GetAllBranchesAsync();
        public Task<BranchModel> GetBranchAsync(int BranchId);
        public Task<int> AddBranchAsync(BranchModel model);
        public Task DeleteBranchAsync(int BranchId);
        public Task UpdateBranchAsync(int BranchId, BranchModel model);
    }
}
