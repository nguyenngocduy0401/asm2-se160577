﻿using Asm2.Data;
using Asm2.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Asm2.Repositories
{
    public class BranchRepository : IBranchRepository
    {
        private readonly EmployeeManagerContext _context;
        private readonly IMapper _mapper;

        public BranchRepository(EmployeeManagerContext context, IMapper mapper) {
            _context = context;
            _mapper = mapper;
        
        }
        public async Task<int> AddBranchAsync(BranchModel model)
        {
            var newBranch = _mapper.Map<Branch>(model);
            _context.Branches!.Add(newBranch);
            await _context.SaveChangesAsync();

            return newBranch.BranchId;
        }

        public async Task DeleteBranchAsync(int BranchId)
        {
            var deleteBranch = _context.Branches.SingleOrDefault(b => b.BranchId == BranchId);
            if (deleteBranch != null) { 
                _context.Branches!.Remove(deleteBranch);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<List<BranchModel>> GetAllBranchesAsync()
        {
            var branches = await _context.Branches!.ToListAsync();
            return _mapper.Map<List<BranchModel>>(branches);
        }

        public async Task<BranchModel> GetBranchAsync(int BranchId)
        {
            var branch = await _context.Branches!.FindAsync(BranchId);
            return _mapper.Map<BranchModel>(branch);
        }

        public async Task UpdateBranchAsync(int BranchId, BranchModel model)
        {
            if (BranchId == model.BranchId) {
                var updateBranch = _mapper.Map<Branch>(model);
                _context.Branches!.Update(updateBranch);
                await _context.SaveChangesAsync();
            }
        }
    }
}
